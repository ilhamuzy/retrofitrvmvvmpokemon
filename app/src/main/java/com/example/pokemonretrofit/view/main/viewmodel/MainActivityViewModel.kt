package com.example.pokemonretrofit.view.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pokemonretrofit.data.model.ResponsePokemonData
import com.example.pokemonretrofit.data.repository.MainRepository

class MainActivityViewModel: ViewModel() {
    val mainRepository = MainRepository()

    val responseData = MutableLiveData<ResponsePokemonData>()
    val errorData = MutableLiveData<Throwable>()

    fun getDataMainViewModel(){
        mainRepository.ShowDataPokemon(
            {
                responseData.value = it
            },
            {
                errorData.value = it
            }
        )
    }
}