package com.example.pokemonretrofit.view.main

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.pokemonretrofit.R
import com.example.pokemonretrofit.data.model.ResponsePokemonData
import com.example.pokemonretrofit.view.main.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity: AppCompatActivity() {

    private lateinit var mainActivityViewModel: MainActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        mainActivityViewModel.getDataMainViewModel()
        setupComponent()
    }

    private fun setupComponent(){
        mainActivityViewModel.responseData.observe(this, Observer {
            showData(it)
        })
        mainActivityViewModel.errorData.observe(this, Observer {
            showError(it)
        })
    }

    private fun showError(it: Throwable?) {
        Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()
    }

    private fun showData(it: ResponsePokemonData?) {
        val adapter = MainPokemonAdapter(it?.results, this@MainActivity)
        rv_main.apply {
            rv_main.layoutManager = GridLayoutManager(this@MainActivity, 2)
            rv_main.setHasFixedSize(true)
            rv_main.adapter = adapter
        }
    }

}