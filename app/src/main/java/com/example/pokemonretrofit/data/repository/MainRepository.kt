package com.example.pokemonretrofit.data.repository

import android.util.Log
import com.example.pokemonretrofit.data.model.ResponsePokemonData
import com.example.pokemonretrofit.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.logging.Handler

class MainRepository {
    fun ShowDataPokemon(responseHandler : (ResponsePokemonData) -> Unit, errorHandler: (Throwable) -> Unit){
        ApiClient.instance.getData().enqueue(object : Callback<ResponsePokemonData>{
            override fun onResponse(
                call: Call<ResponsePokemonData>,
                response: Response<ResponsePokemonData>
            ) {
                if (response.isSuccessful){
                    responseHandler(response.body()!!)
                    Log.d("ResponseData", responseHandler.toString())
                }
            }

            override fun onFailure(call: Call<ResponsePokemonData>, t: Throwable) {
                errorHandler(t)
                Log.d("errorData", errorHandler.toString())
            }
        })
    }

}