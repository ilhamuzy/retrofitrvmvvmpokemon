package com.example.pokemonretrofit.network

import com.example.pokemonretrofit.data.model.ResponsePokemonData
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("pokemon")
    fun getData(): Call<ResponsePokemonData>
}